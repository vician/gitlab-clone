## Installation

	sudo apt install myrepos python3-gitlab

## Run

	./gitlab-clone.py -s https://gitlab.com -t YOUR_SECRET_TOKEN -d ~/Documents/git/gitlab.com/

## Sources

- https://stackoverflow.com/questions/29099456/how-to-clone-all-projects-of-a-group-at-once-in-gitlab
- https://gitlab.com/profile/personal_access_tokens
- https://GITLABURL/api/v4/users/GITLABUSER/projects
