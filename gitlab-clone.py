#!/usr/bin/env python3

import argparse
import os
from subprocess import call
from gitlab import Gitlab

import argparse

parser = argparse.ArgumentParser(description='Clone all of your gitlab projects locally.')
parser.add_argument('-s', '--server', required=True, help='Gitlab server host, e.g.: https://gitlab.com')
parser.add_argument('-t', '--token', required=True, help='Gitlab server token from e.g.: https://gitlab.com/profile/personal_access_tokens')
parser.add_argument('-d', '--dir', required=True, help='Path to target directory, e.g.: ~/Documents/git/')
parser.add_argument('-a', '--archived', required=True, help='Path to target directory, e.g.: ~/Documents/gitarchived/')

args = parser.parse_args()

# Register a connection to a gitlab instance, using its URL and a user private token
gl = Gitlab(args.server, args.token)
#groupsToSkip = ['aGroupYouDontWantToBeAdded']
groupsToSkip = []

gl.auth() # Connect to get the current user
#print(gl.user)

mrconfigArchivePath = os.path.expanduser("~/.mrconfig-archive")


gitBasePath = os.path.expanduser(args.dir)
os.makedirs(gitBasePath,exist_ok=True)

mrconfigArchivePath = os.path.expanduser(args.archived)
os.makedirs(mrconfigArchivePath,exist_ok=True)

for p in gl.projects.list(owned=True, all=True):
    #print(p.archived)
    #print(p.name)
    if p.archived:
        pathToFolder = os.path.join(mrconfigArchivePath, p.name)
        commandArray = ["mr", "-c", mrconfigArchivePath, "config", pathToFolder, "checkout=git clone '" + p.ssh_url_to_repo + "' '" + p.name + "'"]
    else:
        pathToFolder = os.path.join(gitBasePath, p.name)
        commandArray = ["mr", "config", pathToFolder, "checkout=git clone '" + p.ssh_url_to_repo + "' '" + p.name + "'"]
    #print(p.name, pathToFolder)
    call(commandArray)

os.chdir(gitBasePath)

call(["mr", "update"])
call(["mr", "-c", mrconfigArchivePath, "update"])
